+++
title = "Useful Zimbra mailbox management Tips and Tricks"
description = "Some really good zimbra commands i use on a daily basis"
date = "2019-09-27"
tags = [
    "cli",
    "zimbra",
    "zmmailbox",
]
categories = [
    "zimbra",
]
+++

These are some of the commands I use most, or, zimbra commands that were relevant to a very specific goal and i had to document them to avoid losing them :)

<!--more-->

**Get a mailbox folder list with message count:**

    zmmailbox -z -m user@example.net getAllFolders

Most `zmmailbox` command parameters have easy mnemonics like `getAllFolders` can be replaced with `gaf`

**Get mailbox total size:**

    zmmailbox -z -m user@example.net gms

**Empty all messages of a specific mailbox folder:**

    zmmailbox -z -m user@example.net emptyFolder /Inbox

**Tip:** Contacts and Emailed Contacts are also folders so, cleaning up the `/Emailed Contacts` or `/Contacts` folder will clear those categories of contacts.

**Export and compress mailbox(backup):**

    zmmailbox -z -m user@example.net getRestURL "//?fmt=tgz" > /tmp/user_backup.tar.gz

This one is useful if import/export feature is disable globally on your Zimbra server.

**Import a mailbox backup from cli, resolving duplicates**(see options below):

    zmmailbox -z -m user02@example.net postRestURL "//?fmt=tgz&resolve=FOO" /tmp/user_backup.tar.gz

Where `FOO` can be replaced by:

* `skip` = Ignore duplicate items found on backup file
* `modify` = Update current mailbox items with the content of what is being imported from the backup file
* `reset` = Delete old folder and replace the content with backup file content

**Export mailbox contacts as `.csv` file**(syntax may vary depending on your Zimbra version):

    zmmailbox -z -m user@example.net getRestUrl "//?fmt=csv&query=under:Contacts" > /tmp/user_contacts.csv
    zmmailbox -z -m user@example.net -t 0 getRestUrl "//contacts?fmt=csv" > /tmp/user_contacts.csv

You can also change the format(`fmt=`) to `zimbra-csv`, `yahoo-csv`, `thunderbird-csv`, `outlook-2000-csv` or `outlook-2003-csv` if you want to make data more compatible with other applications.

**Clear the dumpster**(trash recovery if any):

    zmmailbox -z -A -m user@example.net emptyDumpster

**Search for emails:**

    zmmailbox -z -m user@example.net s -t message \"Search String\"

**Tip:** You could use `from: address` or `to: address` or `subject: Subject foo bar` for better results than full text searches. Also, the `-l NUM` can be used to limit the command output where `NUM` is the number of lines or result entries you want.

**Create a "spamlover" account:** 

    zmprov ma user@example.net amavisSpamLover TRUE amavisBypassSpamChecks TRUE

This one is useful if you have a mailbox where you need to create filters and automate mail actions(say, [EDI](https://en.wikipedia.org/wiki/Electronic_data_interchange) files) but `[SPAM]` tag gets inserted on some messages preventing them to be delivered correctly to the `/Inbox` folder.

**List mailboxes quota**(do your homework and blend this command with `awk` to convert disk units):

    zmprov qgu `hostname`

Thats it!
