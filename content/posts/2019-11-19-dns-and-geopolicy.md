+++
title = "DNS and Geopolicy: Sometimes, you need to go back to basics"
description = "A random case where Geopolicy screw with DNS"
date = "2019-11-19"
tags = [
    "cli",
    "tcpdump",
    "dns",
]
categories = [
    "dns",
]
+++

There comes a time in life where you enable a high-level protection in your firewall, but you somehow forget the basics and that is where the problem lives..

<!--more-->

First of all, [Geolocation](https://en.wikipedia.org/wiki/Geolocation_software) isn't a new thing. Quoting wikipedia without shame:

> In computing, geolocation software is software that is capable of deducing the geolocation of a device connected to the Internet. The identification of a device's IP address can be used to determine the country, city, or post/ZIP code, determining an object's geographical location. Other methods include examination of a MAC address, image metadata, or credit card information. 

Meaning: Your firewall is probably downloading on a regular basis an IP Database from a paid provider, making relationships between Nets and Countries.

We did block a lot of Countries that had no relationship with our business([Kyrgyzstan](https://en.wikipedia.org/wiki/Kyrgyzstan), i'm looking at you :P ), some of them bidirectionally(input and output) due to a high hit count from bots trying to probe services/ports and to relieve [IPS](https://en.wikipedia.org/wiki/Intrusion_detection_system) loads, since Geo-based traffic is blocked before going to other protections. SMTP is clearly the protocol that enjoys most from this blocking. Problem is, whenever some of our users tried to access specific sites (Latam, Nike), the site layout was messed up, static content and css had some issues, etc.

After spending some time investigating with Firefox Web Developer tools, we have found that only a few domains inside those sites were having DNS resolution errors(Nike:hitbr.acstat.com, LATAM:s.latamstatic.com). Also, using `dig` or `host` shown the correct authority but with empty response.

What protocol works in a hierarchically an decentralized manner? Yeah, [DNS](https://en.wikipedia.org/wiki/Domain_Name_System). And guess the contry where those domains are hosted? China.

We couldn't see the big picture since Nike site URL wasn't blocked, there was too much traffic and firewall log entries to get that only one related to geolocation so, after gathering this info from the browser we used tcpdump at our external Linux dns resolver(cause nobody could be crazy enough to make Active Directory an external resolver, right...), and this was the output:

    [root@nameserver ~]# tcpdump -i eth0 -nevvv udp port 53 | grep hitbr.acstat.com
    tcpdump: listening on eth0, link-type EN10MB (Ethernet), capture size 65535 bytes
        IP.EXTERNAL.RESOLVER.LINUX.63074 > 92.53.116.26.domain: [bad udp cksum 9ccf!] 18512 A? hitbr.acstat.com. (34)
        IP.INTERNAL.RESOLVER.WINDOWS.58666 > IP.EXTERNAL.RESOLVER.LINUX0.domain: [udp sum ok] 62118+ [1au] A? hitbr.acstat.com. ar: . OPT UDPsize=4000 OK (45)
        IP.EXTERNAL.RESOLVER.LINUX.domain > IP.INTERNAL.RESOLVER.WINDOWS.58666: [bad udp cksum b1e1!] 62118 ServFail q: A? hitbr.acstat.com. 0/0/1 ar: . OPT UDPsize=1252 OK (45)
        IP.EXTERNAL.RESOLVER.LINUX.domain > IP.INTERNAL.RESOLVER.WINDOWS.58713: [bad udp cksum edd9!] 64059 ServFail q: A? hitbr.acstat.com. 0/0/1 ar: . OPT UDPsize=1252 OK (45)
        IP.EXTERNAL.RESOLVER.LINUX.domain > IP.INTERNAL.RESOLVER.WINDOWS.50785: [bad udp cksum 2607!] 63908 ServFail q: A? hitbr.acstat.com. 0/0/0 (34)    
        IP.INTERNAL.RESOLVER.WINDOWS.50679 > IP.EXTERNAL.RESOLVER.LINUX.domain: [udp sum ok] 30465+ A? hitbr.acstat.com. (34)

**Step-by-step:**

*  Probe who owns `hitbr.acstat.com`
*  Finds out its `92.53.116.26`
*  Tries to contact authority, get blocked by Geopolicy firewall rules(cause it's China...)

Here you can see our external DNS answering to AD that it couldn't reach the authoritative servers of that domain, and after that, AD did another request and the output repeats.

`SERVFAIL` DNS messages are used when the fully qualified domain name (FQDN) that has been looked up does exist, that the root name servers have information on the domain but that the authoritative name servers are not answering queries for this domain. 

And just out of pure curiosity, s.latamstatic.com also have it's athoritative DNS in china. Some countries we didn't block bidirectionally like Netherlands cause important stuff is hosted there like cloud FTP servers we connect to, but we didn't remember that companies could be hosting some of their DNS in China.

**tl,dr:** Block input from countries you do not wish to communicate. Block output only on specific cases. There is much more in this world than BitTorrent as a distributed protocol to be blocked on corporate environment.