+++
title = "Generating QR Codes for your Guest wifi on OpenWRT"
description = "A quick guide + script to create qrcodes for your guest wifi"
date = "2022-02-24"
tags = [
    "openwrt",
    "wifi",
]
categories = [
    "openwrt",
    "wifi",
]
+++

It's been a long time since I use [OpenWRT](https://openwrt.org) on my home routers, and prefer to buy a router that could be a little outdated but has support to this wonderful firmware.

This is a quick tutorial on how to automate qr code generation and password rotation for your OpenWRT-based devices.

<!--more-->

What is this blogpost about
====================

* Using `qrencode` to generate QR Codes that will be later feed to a CGI script to be further presented to a web browser
* The script that will do the password rotation.
* `cron` job and some backup related configurations.

Requirements
====================

* OpenWRT already installed on your device. This isn't a tutorial on how to install this resource on your home router
* Multiple SSIDs already segregated into different bridges/vlans or whatever other method you are using. This tutorial will not guide you through full wireless configuration
* Some basic `ssh` and scripting knowledge

Installing packages and preparing OpenWRT
====================

**Step 01**: Install the `qrencode` package:
```
opkg update
opkg install qrencode

``` 

**Step 02**: Create the following script which will read data from `/dev/urandom` and replace it with `tr` getting the first 12 characters. I have named it `/root/rotate_guest_wifi_password.sh`
``` 
#!/bin/sh

password=`cat /dev/urandom | env LC_CTYPE=C tr -dc _ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjklmnpqrstuvwxyz23456789- | head -c 12; echo;`

echo $password > /root/.guest_password.txt

ssid=Dragons
security=WPA2
qrencode -t SVG -o /www/wifi.svg "WIFI:S:$ssid;T:$security;P:$password;;" 

uci set wireless.wifinet2.key=$password
uci commit wireless
wifi
``` 

Change the SSID value to one that reflect your networks. I have `Dungeons` as my Main SSID, and `Dragons` as the Guest wifi.

And as you may have noticed, I'm not using a lot of special characters on this password to keep things simpler for those who eventually use my wifi. It's a "good enough" measurement to avoid passing my Main SSID password to visits.

I've also blocked all forwarding between Main and Guest SSIDs, all forwarding from Guest SSID to the internet except for http and https(80 and 443), and input from Guest SSID to router is only allowed for dns and dhcp.

**Step 03**: Crate file named `/www/cgi-bin/guest` which will later be accessed by a smartphone or any other device used as display for the qrcode.

``` 
#!/bin/sh

password=$(cat /root/.guest_password.txt)

echo "Content-Type: text/html"
echo ""
echo "<!DOCTYPE html>"
echo '<html lang="en-US">'
echo "<head>"
echo "<title>Guest Password</title>"
echo '<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">'
echo '<meta http-equiv="refresh" content="360" />'
echo "</head>"
echo '<body bgcolor="#000">'
echo "<div style='text-align:center;color:#fff;font-family:UnitRoundedOT,Helvetica Neue,Helvetica,Arial,sans-serif;font-size:28px;font-weight:500;'>"
echo "<h1>Guest WIFI</h1>"
echo "<p>Network: <b>Dragons</b></p>"
echo "<p>Password: <b>$password</b></p>"
echo '<img src=../wifi.svg style="width:50%"></img><br>'
echo "</div>"
echo "</body>"
echo "</html>"
``` 

Again, replace "Network:" with the name of your Guest wifi, and change HTML acording with your needs(or make a less ugly one if you will and know how to do that...). This page will also show the current password in plain mode for those older smartphones which lack of qr code scanning features, or for laptops.

**Step 04**: Set executable permissions to both the wifi password rotation script and the cgi sript:
```
chmod +x /www/cgi-bin/guest
chmod +x /root/rotate_guest_wifi_password.sh 
```

**Step 05**: Add those files to `/etc/sysupgrade.conf` file so they will not be removed during OpenWRT version upgrades.
```
echo "/www/cgi-bin/guest" >> /etc/sysupgrade.conf
echo "/root/rotate_guest_wifi_password.sh" >> /etc/sysupgrade.conf 
```

**Step 06**: Add your script to the root user crontab at `/etc/crontabs/root` using `vi` or through the luci-web interface, following the user crontabi column standards for time and dates:
```
root@hellwrt:~# cat /etc/crontabs/root 
30 01 01 * * /root/rotate_guest_wifi_password.sh
```

I've configured password rotation once in a month(day 1, 01:30) instead of once in a week(all Sundays). It was really painfull for some relatives that come over my house frequently and had to keep their smartphones forgetting and re-adding this network all over again.

**Step 07**: Manually execute the script once to make sure you have a brand new password and qr code for your wifi:
```
/root/rotate_guest_wifi_password.sh
```

Note that this will also create a small hiccup on your wifi network due to the reload/refresh actions that will be taken to apply the new configurations


**Step 08**: Add a bookmark or direct link on your smartphone home screen to `https://your.router.ip.addr/cgi-bin/guest` to keep things easy :)

In my case, since I'm filtering the input on `wan` and `br-guest` interfaces, this page will only be available for devices already connected at the Main SSID as expected.

![Guest wifi CGI page](/img/2022-02-24/guest.png)

Be safe!
