+++
title = "How to create a good Mate"
date = "2019-09-24"
tags = [
    "mate",
    "chimarrão",
]
categories = [
    "gaucho",
]
+++

Yeah, you are not misreading it. I'm not talking about [this](https://en.wikipedia.org/wiki/MATE_(software)) Mate. I'm talking about [this](https://en.wikipedia.org/wiki/Mate_(drink)) one ;)

Making a decent chimarrão(mate) is essential to keep you hydrated during the cold winter, and making a not clogged chimarrão is pure art.
<!--more-->

Here is what you need:

* Cuia: The recipient where you will do the infusion
* Erva (yerba): the tea-like(IT's NOT TEA) infusion powder
* [Bomba](https://en.wikipedia.org/wiki/Bombilla): The metal straw with little holes on the lower end that will be filled with water
* Cold water: To start the infusion
* Hot water: To drink. It's not boiled water(about 85 ºC)
* Your hands or a piece of plastic(pot lid, maybe), piece of wood: To keep the erva inside your cuia while you shake it to settle it. 

**Step 1**: Fill three-quarters of your cuia with erva(yerba). Rule of thumb is: almost all cuias have this curve at the "neck" where the body is thinner, and that is the 3/4 mark.

![Erva-filled cuia](/img/2019-09-24/cuia_erva.svg)

**Step 2**: Seal the opening with your hand or with the plastic piece, but **DO NOT** turn it upside down.

Just make a little angle towards the ground and gently shake it until you feel the erva got somehow compact.

![Preparation](/img/2019-09-24/cuia_virada01.svg)

**Step 3**: Rotate up the cuia a little and take care to avoid making an erva avalanche/landslide. If this happens, repeat step 2. 

**Step 4**: Start to put cold water making it flow through the inner side of the cuia, without touching directly the erva. This is a key step to make a chimarrão that is not clogged.

![Carefully filling the water 01](/img/2019-09-24/cuia_virada02.svg)

![Carefully filling the water 02](/img/2019-09-24/cuia_virada03.svg)

**Step 5**: When the water reach the border of the erva, you start to rotate the cuia slowly to the upright position while filling the remaining space with cold water.

**Step 6**: Insert the bomba(straw) into the water with your thumb sealing the top hole, creating some pressure and avoiding erva particles to fill the straw. 

Rotate it to be aligned with the erva, and get your thumb out. You should listen to a little "pop" sound.

**Step 7**: It's up to you to let the chimarrão rest until all cold water is absorved by the erva, or suck all cold water and spit it on your sink to accelerate the process.

**Step 8**: Fill with hot water, drink it, serve again, pass the mate to the next person (tradition says the sequence goes anti clockwise).

That's it!

