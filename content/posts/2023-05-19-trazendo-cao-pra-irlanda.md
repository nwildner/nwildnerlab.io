+++
title = "Como eu trouxe meu cachorro pra Irlanda"
date = "2023-05-19"
tags = [
    "dog",
    "pet",
    "cachorro",
    "documentação",
    "pt_BR",
]
categories = [
    "pets",
]
+++

Este é um pequeno grande relato de como trouxe meu cão, o Beico(isso, misspelled Bacon ou Bacon abrasileirado) para a Irlanda.

De antemão aviso que este não é um processo fácil e neste artigo tentarei fazer um passo a passo do processo de "importação" do nosso doguinho relatando minha própria experiência.

Este guia se restringe ao transporte de cães na cabine visto que foi o processo que fiz já que cães [braquicefálicos](https://www.petz.com.br/blog/cachorros/caes-braquicefalicos/) não são permitidos no compartimento de cargas. Também não entrarei em detalhes relacionados a transporte de pets por terceiros pois eu mesmo trouxe o cachorro e o processo de vir com outro tutor possui umas diferenças, apesar de a grosso modo o procedimento do Certificado Veterinário Internacional ser feito através do mesmo recurso online.

Este relato será ora em primeira pessoa e ora em terceira, quando eu achar que as informações devem ser direcionadas em forma de instrução para aqueles que lêem este blog.

<!--more-->

Requisitos
====================

Estes são os pré-requisitos básicos para que seu pet [entre na Irlanda](https://www.citizensinformation.ie/en/moving-country/moving-to-ireland/coming-to-live-in-ireland/bringing-pets-to-ireland/).

* Microchip padrão ISO 11784/85, FDX-B 134.2 KHz. Meu veterinário indicou o BackHome da Virbac por ser um dos mais utilizados.
* Vacinação da raiva em dia
* Sorologia da raiva. Este exame indica que o pet está "imune o suficiente" para entrar na União Européia.
* Passando na sorologia, quarentena de 90 dias(3 meses) no Brasil a contar da data da coleta de sangue para a sorologia.
* Tratamento para o Echinococcus multilocularis, aplicado a no máximo 5 dias antes da data de chegada na Irlanda. Este prazo é importante pois "casa" com o prazo de emissão do atestado sanitário AS-1 no Brasil.
* Travel notice. No caso para o aeroporto de Dublin, é um email enviado para petmove@agriculture.gov.ie. Portos e Aeroportos de outras localidades na Irlanda responderão por outras caixas de email.
* Travel notice em qualquer outro país que você venha a fazer escala.
* Compliance check em Dublin ou caso haja escala em outro país membro da União Européia, este mesmo procedimento feito naquele país. A Irlanda aceita o resultado do compliance check de outro país membro da UE, mas isto não elimina a necessidade do tratamento anti parasitário já que alguns países do bloco não necessitam.

Outros detalhes relativos a viagem:

* Pet na cabine pode ter no máximo 8KG com a mala de viagem dele. Esse é o peso máximo pra vôos pela TAP, e em vôos LATAM+Lufthansa esse valor é menor(7KG)
* A passagem de Pet precisa ser comprada por telefone, e você precisa passar detalhes do cartão de crédito por telefone. O pagamento não pode ser parcelado
* Verifique as dimensões da mala de transporte do cão. Na época da viagem o valor declarado no [site da TAP](https://www.flytap.com/en-cv/travelling-with-animals/pets?tabCode=cabin-transport) era de 40x33x17cm
* Compre um leitor de chip "just in case" e teste no seu cachorro. O cão pode ser deportado caso o chip não leia. Compramos [este modelo](https://www.amazon.co.uk/dp/B0BR3L16L7)

Antes de Partir
====================

A história começa no fatídico dia 27/JUN/22 quando tive uma tremedeira nas pernas após o HR de uma vaga que eu estava concorrendo me ligar e explicar que fui selecionado para uma vaga na Irlanda. Eu já não estava mais contando com esta vaga, dado o tempo que levaram pra me contactar o que tornou a surpresa ainda maior.

Como eu tinha a convicção de que não havia sido selecionado, vacinei meu cão em Abril do mesmo ano contra a raiva. Guarde esse detalhe.

Liguei para alguns veterinários auto denominados "especialistas" no processo de imigração de pet pra Europa, pra Vigiagro e enviei alguns emails para empresas que faziam o transporte de pets e das duas uma: ou dava pra notar que a pessoa não sabia(demonstrava não saber), ou o valor para fazer todo o processo de transporte do pet para a Irlanda era demasiado caro. Resolvi estudar o caso e fazer eu mesmo todos os passos.

Depois de lidar com papelada e outras documentações relativas a vaga de trabalho e visto, resolvi implantar o microchip no cão e quem sabe já fazer a coleta de sangue para a sorologia. Ao chegar no veterinário fui informado da seguintes condições:

* O Beico não poderia coletar sangue que seria enviado para a sorologia porque esta coleta precisava ser feita perto de um mês depois da vacina. Eu fui na palavra do veterinário apesar de hoje saber que esta coleta pode ser feita a qualquer momento se você vacina regularmente seu cão todos os anos. Essa informação eu não tinha na época portanto, segui o que o profissional da área indicou.
* Ele não podia receber uma nova dose da vacina em menos de 5 meses(lembra, Abril?) porque havia risco de morte para braquicefálicos a aplicação de uma nova dose da vacina. Essa informação foi confirmada por um funcionário da Virbac, laboratório da vacina.

Beico teve o chip implantado em Julho, em Setembro fez a vacina da raiva para que em Outubro um familiar conduzisse ele novamente a clínica veterinária para a coleta sanguínea.

Como viríamos para a Irlanda no início de Outubro, ficou com um parente a tarefa de acompanhar a coleta de sangue e também manter o Beico "nos eixos" da sua dieta. Ele estava um tanto obeso e entrou numa dieta rigorosa, com comida regrada e bastante exercício. No tempo que ficou no Brasil emagreceu de 14,9KG para 7KG(8KG cravados com a mala).

Já na Irlanda
====================

No início de Outubro já estávamos na Irlanda e quem conduziu a coleta de sangue foi um parente, com os detalhes todos acertados. Sorologia chegou e o pet estava dentro dos conformes nos parâmetros para a viagem.

Agora era só acompanhar a perda de peso do Beico e aguardar março de 2023 quando iríamos dar um pulinho no Brasil para visitar a família e trazer o pet.A ansiedade era gigante e a quantidade de material que li sobre o processo de levar o pet pra Europa foi grande.

Em Fevereiro enviei um email para o orgão citado no início deste blogpost agendando a vistoria sanitária(compliance check) do Beico e informando os detalhes do meu vôo vindo do Brasil com escala no Canadá.

Preparando para a volta
====================

Deixarei fora deste relato a história da tentativa de vir pelo Canadá, que falhou miseravelmente visto que até hoje estou esperando o maldito Transit Visa, um visto de pouco poder e serve só pra transitar dentro do aeroporto.

Por conta da situação do visto Canadense ainda pendente, compramos a volta pela TAP e cancelamos a passagem da Air Canada. Foram horas excruciantes para comprar a passagem de pet pelo telefone mas deu certo. A viagem foi de 34 horas, já contando as 4+ do Fuso horário e as 11 horas de stopover em Lisboa.

Por conta desta mudança, mandei um novo email para o PetMove, caixa de email da fiscalização do aeroporto de Dublin. 

Segue o template de email caso você queira utilizar na sua viagem:

```
Subject: 
Bringing dog to Ireland, DD/MM - ____Full name_____ 

Body:
Hello Officer/Agent.

My name is _______________, and I'll be bringing my dog(__Breed__ - Male) from Brazil to Ireland.

The arrive date is __ _____ 2023, and I'll be arriving at __:__ - Dublin Airport.

The dog Microchp is _______________, and he passed the serological Rabies Test and since he was vaccinated last Sep, the 90 days of quarantine were accomplished.

I'll be handling the tapeworm treatment before the flight, and this dog will travel ON CABIN.

Thanks in advance.

```

Recebi então um .DOC em resposta ao email para que eu preenchesse mais alguns dados adicionais meus e do cachorro.

Como a hora prevista de chegada na Irlanda era as 00:10 e o horário de inspeção vai das 07:30 as 21:00, MON-FRI, eu teria que fazer o compliance check com um plantonista terceirizado que cobraria 350 euros pelo serviço. Ao informar que eu viria por escala em Lisboa, este plantonista foi bastante honesto e afirmou que membros da UE aceitam compliance check de outros países membros se houvesse uma escala prévia. Essa informação valiosa me poupou vários euros(paguei 40 EUR em Lisboa), e poupou o transtorno de ter que fazer uma inspeção no final da viagem tendo que levar malas.

Enviei então um email para as autoridades de Lisboa conforme o que está escrito no [seguinte site da DGAV](https://www.dgav.pt/vaiviajar/conteudo/animais-de-companhia/transito-internacional/entrada-em-portugal/caes-e-gatos/aviso-de-chegada-como-fazer/) - Direção Geral de Alimentação e Veterinária, agendando a inspeção durante meu stopover. 

Consulte o aeroporto onde você fará escala para maiores detalhes. No aeroporto de Lisboa, a sessão de vistorias fica perto da área de baggage claim, no final a esquerda. Você pode ir lá e depois retornar para a área de conexões, mas no meu caso como eram 11 horas de stopover, aproveitei para sair do aeroporto e almoçar fora, dar uma caminhada com dog e voltar mais tarde para a área de embarque.

CVI - Certificado Veterinário Internacional
====================

Esse é um rolê complicado, mas hoje em dia está até mais fácil visto que o procedimento é totalmente online, enquanto que antigamente era necessário ir em recorridas visitas a Vigiagro para estes passos descritos abaixo. Atualmente, apenas uma visita algumas horas antes da viagem é necessária caso o país destino exija a chancela física do documento.

É importante que antes de começar este processo que você tenha uma conta ["Gold" no Gov.Br](https://www.gov.br/governodigital/pt-br/conta-gov-br/saiba-mais-sobre-os-niveis-da-conta-govbr) para ter acesso a alguns dos serviços que serão necessários nessa etapa como a assinatura digital dos PDFs.

Apesar de alguns lugares na internet e até mesmo sites de governos falarem do Atestado Sanitário(AS-1), que é o documento contendo dados do animal e carimbado pelo seu veterinário de confiança, você **NÃO PRECISA sair imprimindo e preenchendo tal atestado**. Assim que o processo de pedido do CVI for iniciado, um atestado inicial será gerado para você levar ao veterinário de confiança que aplicará o antiparasitário. Não é necessário baixar modelos prontos da internet que seguem o padrão UE, ou até mesmo a versão em branco do AS-1 do portal Gov.BR, pois este era usado no passado quando o processo era todo offline.

Para a Irlanda e alguns outros países, o documento final precisa ser chancelado(carimbo físico) no dia da viagem então, fique atento aos prazos. 

Esta é a razão para iniciar o pedido do CVI 3 dias antes da viagem: A Irlanda precisa receber o pet com no máximo 5 dias depois de aplicado o antiparasitário enquanto que no Brasil o antiparasitário precisa de 24h para agir + 2 dias do prazo de emissão do CVI. Recomendo que você consiga um vôo com menos de 48 horas. No meu caso, havia opções de passagem muito baratas com stopover de 17 horas em LIS, contudo, optei por um com "apenas 11 horas" de chá de cadeira no aeroporto...hehehe.

Escaneie os seguintes documentos:

* Bilhete Eletrônico da sua viagem. Print de tela de celular não vale. Precisa ser o bilhete eletrônico em PDF.
* Certificado de Microchipagem assinado e carimbado pelo veterinario(PDF).
* Documento da Sorologia(PDF).
* Carteira de vacinação do cachorro com informações de microchip devidamente preenchidas e a página que consta o endereço da clínica veterinária(PDF).

Tenha certeza de que o escaneamento não saiu borrado ou com informações ausentes/cortadas pois o agente lidando com seu processo pode pedir a correção de algum documento que ele julgar incompleto.

Realize a [Assinatura Digital](https://www.gov.br/pt-br/servicos/assinatura-eletronica) de todos os documentos acima. Tome o cuidado para não posicionar a assinatura digital em cima de alguma informação sensivel do documento pois seu processo pode retornar pedindo para corrigir isto também

Três dias antes da sua viagem e algumas horas antes de você aplicar o tratamento antiparasitário(Echinococcus multilocularis) comece o processo do Certificado Veterinário Internacional. Os passos são:

* Acesse o site específico do CVI para União Européia, Irlanda do Norte, Noruega e Suiça [aqui](https://www.gov.br/pt-br/servicos/solicitar-certificado-veterinario-internacional-para-viajar-com-seu-cao-ou-gato-para-uniao-europeia-irlanda-do-norte-noruega-e-suica-cvi). 
* Preencha todos os dados relacionados a você, seu vôo, seu pet, e campos que exijam upload da Sorologia Antirrábica, Carteira de Vacinação, Bilhete do Vôo assinados previamente.
* No campo observações gerais, coloque algum detalhe que achar interessante. Como sempre, seja educado :)
* Ao clicar em Próximo, o Atestado de Saúde AS-1 será gerado com os dados do Pet já preenchidos. Verifique se todos os dados do pet estão corretos e imprima uma cópia.
* Vá ao veterinário com o AS-1, aplique o tratamento antiparasitário, preencha seus dados e os do veterinário bem como a assinatura e carimbo do profissional.
* Volte pro seu computador, escaneie o AS-1 preenchido e carimbado, faça o processo de assinatura e prossiga com o preenchimento do formulário no Gov.Br.
* A qualquer momento do processo de solicitação do CVI, você pode cliar no botão "Salvar Formulário" no canto superior direito para que os dados preenchidos até o momento continuem ali.

A partir daí está nas mãos do agente responder seu pedido. Sei que os prazos são apertados e a gente fica com o coração na mão, por isso, fique de olho no seu email para monitorar pedidos de correção por parte dos agentes. 

No meu caso, pediram pra escanear novamente a carteira de vacinação pois não havia o endereço da clínica veterinária. Escaneei a carteira de vacinação com a capa e verso que eram os locais que continham o endereço e nome da clínica, assinei digitalmente mais uma vez e no campo observações com toda educação possível expliquei que havia escaneado tais folhas contendo o endereço.

Se tudo ocorrer certo, o dossiê para a viagem chega no dia anterior ao embarque. Esse dossiê é um compilado de TODOS esses documentos num pdf único, e que possui um código digital na lateral com 16 caracteres separados em 4 grupos de 4 dígitos.

Munido deste documento(nem adianta ligar antes do dossiê completo) ligue na Vigiagro do aeroporto em que você irá embarcar, e explique que precisa da chancela física do documento pra viagem pra Irlanda. No final do processo do CVI há uns links internos com telefones e a lista de unidades da Vigiago atualizadas no Brasil.

E isto é tudo. Dei uma canseira no Beico antes da viagem, dei água com água de melissa para ele e ele se comportou muito bem a bordo. Este cão rabugento já está totalmente adaptado ao clima Irlandês e agora tem até  uma capa de chuva:

![Beico e sua capa de chuva](/img/2023-05-19/beico.png)

Espero que este relato lhe ajude :)

Stay safe
