+++
title = "Dell removed S3 (aka Suspend to RAM) from laptops and Windows is the one to blame"
description = "How a Windows bug and a Microsoft market move will end with S3"
date = "2024-06-05"
tags = [
    "dell",
    "laptops",
    "suspend",
]
categories = [
    "laptops",
]
+++

Dell XPS laptops are great for mobility and they are really silent even with load. Got one for work and this laptop is an absolute beast when it comes to battery savings during the day and hardware support. Ubuntu 24.04 recognized all the hardware including the fingerprint reader built-in into the power button.

But one thing was not that great: battery consumption after closing the laptop lid.

<!--more-->

Battery drain
====================

From one day to another, battery drained around 15% from the time I've closed the lid at the end of my shift until the next morning around 09:00 AM.

Knowing what could be the cause which is, the default `s2idle` suspend mechanism being poorly optimized because I've also noticed that on my personal laptop(with around 8% of battery drain), I tried to apply the `deep` keyword into the `/sys` file which holds [ACPI Sleeping States](https://uefi.org/htmlspecs/ACPI_Spec_6_4_html/16_Waking_and_Sleeping/sleeping-states.html) on Linux.

```
[root@dell ~]# echo 'deep' > /sys/power/mem_sleep 
-bash: echo: write error: Invalid argument
```

Oops. Looks like I've missed something, let's see the file contents for the available keywords: 

```
[root@dell ~]# cat /sys/power/mem_sleep 
[s2idle] 
```

That's weird, I should check out my personal laptop:

```
[root@sandworm ~]# cat /sys/power/mem_sleep 
s2idle [deep]
```

Ok. Looks like there is some Firmware(EFI) configuration that needs to be made. Sometimes the most random stuff can make S3 Sleeping state (aka `deep`) unavailable, and I tried to change some of those.

The Bug
====================

I've spent some time tinkering stuff on the Dell laptop firmware to try to enable S3, some of them were:

* Setting different power profiles
* Changing the disk mode from Raid(Intel RST) to AHCI and back to Raid
* Enabling and disabling virtualization related processor options
* Entirely disabling the NVIDIA GPU
* Changing memory power profiles
* Enabling and disabling Secure Boot
* Other minor configs that from previous experiences can disable S3 on laptops
* Upgraded firmware to a more recent version
  * At least Dell provides an Over-the-Air firmware update experience that is really neat. The UEFI connected to the Wifi, downloaded and verified all the files and patched my firmware hassle-free

After that I've got convinced that S3 was not availble on my laptop, and this was a bug that Dell would soon fix, and for my surprise it was "a feature".

The "Feature"
====================

Looks like Windows 10 is having a bad time when waking up from S3, and end up killing itself(unlike Epstein who didn't do that) in a BSOD. One person at the Dell forums opened up a case with Dell Support and got [the following response](https://www.dell.com/community/en/conversations/linux-general/dell-latitude-5420-5520-s3-suspend-deep-sleep-not-working-on-ubuntu-2104/647f950ef4ccf8a8de77bc2f?commentId=647f9558f4ccf8a8de7db56f):

> I reached to Dell support and they told me that they don't support S3 simply because Windows 10 may have BSOD after wake. They don't care about other OS and they don't give customers a choice.
> 
> I used Ubuntu only for testing, since 5420 if officially Ubuntu certified. Apparently certification doesn't mean anything, since the laptop looses its mobility and always have to be plugged to charger.
> 
> I also tried with Debian 11 bullseye and the situation is the same. Which is expected since S3 is blocked in on a low-level BIOS.
> - Quoting user svlasov at July 28th, 2021 07:00

I've got a little upset because, that is not how you should deal with your customers. You are completely removing a feature from your hardware because of some major incompetence of Windows that can't handle wakeup from RAM, which is a pretty common feature of ACPI for the last decades.

The Future
====================

But wait, there is more. 

After doing some research, it looks like Windows 10 and newer will deliberately stop to support S3 anyways. That's because they will favor the shinny new feature called [Modern Standby](https://learn.microsoft.com/en-us/windows-hardware/design/device-experiences/modern-standby) and that should be the one adopted by OEMs. It is an extension of a feature from Windows 8.1 and they try to sell it as if S3 is "limiting" and "traditional" and trying to gaslight S3 into the "old tech field" that only had it's usefulness back in the spinning rust times.

So yeah, looks like a conspiracy and smells like a conspiracy. S3 will soon be dead on other hardware vendors because Microsoft decided that everyone should adopt this modern standby which is basically the default idle mechanism + suspend all userspace activity. Not only that but trying to help with the S3 suspend configuration on a friend's laptop today, we've also found that T14 Gen 2 from Lenovo also got that removed at the firmware level.

Source: Lenovo Foruns - [Enable S3 sleep for T14 Gen 2?](https://forums.lenovo.com/t5/ThinkPad-T400-T500-and-newer-T-series-Laptops/Enable-S3-sleep-for-T14-Gen-2/m-p/5072598)

And what about your Dell XPS?
====================

Luckly, Linux already implemented that very same modern standby on Kernel 3.9 back in 2013 and it is called "suspend-freeze". Quoting the [KernelNewbies](https://kernelnewbies.org/Linux_3.9#New_.22suspend-freeze.22_suspend_state:) website:

> 1.8. New "suspend-freeze" suspend state:
> 
> This release includes a new suspend mode, suspend-freeze. It equals to frozen processes + suspended devices + idle processors. This state is useful for 1) platforms that do not have STR, or have a broken STR 2) platforms that have an extremely low power idle state which can be used to replace STR
> 
> Compared with memory suspend, suspend-freeze saves less power because the system is still in a running state, but has less resume latency because it does not touch BIOS and the processors are in idle state. Compared with RTPM/idle, suspend-freeze saves more power because the processor has longer sleep time once the processes are frozen, and it can get more power saving from the devices that does not have good RTPM support.
> 
> To enable this suspend mode, echo freeze > /sys/power/state
> 
> Code: [(commit)](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7e73c5ae6e7991a6c01f6d096ff8afaef4458c36)

It was engineered to deal with buggy hardware(surprise, surprise) but it became a feature from the modern suspend standpoint.

And how to apply that? Simply create the `/etc/systemd/sleep.conf.d/` directory and paste the contents with your favorite editor into that file(as root):

```
nwildner@dell:~$ cat /etc/systemd/sleep.conf.d/freeze.conf 
[Sleep]
SuspendState=freeze
```

That combined with the `HandleLidSwitch=suspend` parameter on `/etc/systemd/logind.conf` and you are good to go.

After these changes, the daily battery drain in suspend-freeze is about 3 to 5% only. A nice improvement.

Conclusions
====================

* Windows is buggy on S3 wakeup, and that might not get fixed
* Microsoft is forcing their modern suspend to laptop vendors and processors and that will not change
* Linux Distributions are not adopting suspend-freeze by default in the hope that S3 is still available on hardware out there

Guess this suspend-freeze will soon become the default suspend method for Linux on laptops, when the majority of vendors get rid from the good old S3 APCI Suspend state.

That's all. Stay safe! 
